/*
 *  kernel/kernel.c
 *
 *  Minikernel. Versi�n 1.0
 *
 *  Fernando P�rez Costoya
 * Valentino Lin
 * ChengJian Li
 *
 */

/*
 *
 * Fichero que contiene la funcionalidad del sistema operativo
 *
 */

#include "kernel.h"	/* Contiene defs. usadas por este modulo */
#include <stdio.h>
#include <string.h>

int DEBUG = 0;
int DEBUGCREARMUTEX = 0;

int reloj = 0;

/*
 *
 * Funciones relacionadas con la tabla de procesos:
 *	iniciar_tabla_proc buscar_BCP_libre
 *
 */

/*
 * Funci�n que inicia la tabla de procesos
 */
static void iniciar_tabla_proc() {
    int i;

    for (i = 0; i < MAX_PROC; i++) {
        tabla_procs[i].estado = NO_USADA;
    }
}

/*
 * Funci�n que busca una entrada libre en la tabla de procesos
 */
static int buscar_BCP_libre() {
    int i;

    for (i = 0; i < MAX_PROC; i++)
        if (tabla_procs[i].estado == NO_USADA) {
            return i;
        }
    return -1;
}

/*
 *
 * Funciones que facilitan el manejo de las listas de BCPs
 *	insertar_ultimo eliminar_primero eliminar_elem
 *
 * NOTA: PRIMERO SE DEBE LLAMAR A eliminar Y LUEGO A insertar
 */

/*
 * Inserta un BCP al final de la lista.
 */
static void insertar_ultimo(lista_BCPs *lista, BCP * proc) {
    if (lista->primero == NULL) {
        lista->primero = proc;
    }
    else {
        lista->ultimo->siguiente = proc;
    }
    lista->ultimo = proc;
    proc->siguiente = NULL;
}

/*
 * Elimina el primer BCP de la lista.
 */
static void eliminar_primero(lista_BCPs *lista){

    if (lista->ultimo == lista->primero) {
        lista->ultimo = NULL;
    }
    lista->primero = lista->primero->siguiente;
}

/*
 * Elimina un determinado BCP de la lista.
 */
static void eliminar_elem(lista_BCPs *lista, BCP * proc) {
    BCP *paux = lista->primero;

    if (paux == proc)
        eliminar_primero(lista);
    else {
        for ( ; ((paux) && (paux->siguiente != proc));
            paux = paux->siguiente);
        if (paux) {
            if (lista->ultimo == paux->siguiente)
                lista->ultimo = paux;
            paux->siguiente = paux->siguiente->siguiente;
        }
    }
}

/*
 *
 * Funciones relacionadas con la planificacion
 *	espera_int planificador
 */

/*
 * Espera a que se produzca una interrupcion
 */
static void espera_int() {
    int nivel;

    printk("-> NO HAY LISTOS. ESPERA INT\n");

    /* Baja al m�nimo el nivel de interrupci�n mientras espera */
    nivel = fijar_nivel_int(NIVEL_1);
    halt();
    fijar_nivel_int(nivel);
}

/*
 * Funci�n de planificacion que implementa un algoritmo FIFO.
 */
static BCP * planificador() {
    while (lista_listos.primero == NULL) {
        espera_int();		/* No hay nada que hacer */
    }
    return lista_listos.primero;
}

/*
 *
 * Funcion auxiliar que termina proceso actual liberando sus recursos.
 * Usada por llamada terminar_proceso y por rutinas que tratan excepciones
 *
 */
static void liberar_proceso() {
    BCP * p_proc_anterior;

    liberar_imagen(p_proc_actual->info_mem); /* liberar mapa */

    p_proc_actual->estado = TERMINADO;
    eliminar_primero(&lista_listos); /* proc. fuera de listos */

    /* Realizar cambio de contexto */
    p_proc_anterior = p_proc_actual;
    p_proc_actual = planificador();

    printk("-> C.CONTEXTO POR FIN: de %d a %d\n",
            p_proc_anterior->id, p_proc_actual->id);

    liberar_pila(p_proc_anterior->pila);
    cambio_contexto(NULL, &(p_proc_actual->contexto_regs));
        return; /* no deber�a llegar aqui */
}

/*
 *
 * Funciones relacionadas con el tratamiento de interrupciones
 *	excepciones: exc_arit exc_mem
 *	interrupciones de reloj: int_reloj
 *	interrupciones del terminal: int_terminal
 *	llamadas al sistemas: llam_sis
 *	interrupciones SW: int_sw
 *
 */

/*
 * Tratamiento de excepciones aritmeticas
 */
static void exc_arit() {

    if (!viene_de_modo_usuario()) {
        panico("excepcion aritmetica cuando estaba dentro del kernel");
    }

    printk("-> EXCEPCION ARITMETICA EN PROC %d\n", p_proc_actual->id);
    liberar_proceso();

        return; /* no deber�a llegar aqui */
}

/*
 * Tratamiento de excepciones en el acceso a memoria
 */
static void exc_mem() {

    if (!viene_de_modo_usuario()) {
        panico("excepcion de memoria cuando estaba dentro del kernel");
    }

    printk("-> EXCEPCION DE MEMORIA EN PROC %d\n", p_proc_actual->id);
    liberar_proceso();

        return; /* no deber�a llegar aqui */
}

/*
 * Tratamiento de interrupciones de terminal
 */
static void int_terminal() {
    char car;
    BCP *proc = lista_bloqueados_caracter.primero;

    car = leer_puerto(DIR_TERMINAL);
    printk("-> TRATANDO INT. DE TERMINAL %c\n", car);

    if (num_caract_leidos == TAM_BUF_TERM) {
        printk("(INT_TERMINAL) Info: El buffer esta lleno, ignorando caracter...\n");
    } else {
        int posicion = (primero + num_caract_leidos) % TAM_BUF_TERM;
        buffer_terminal[posicion] = (int) car;
        printk("(INT_TERMINAL) Info: Se ha insertado el caracter = %c, posicion = %d\n)", car, posicion);
        num_caract_leidos++;
    }
    // Ya hay letras que tratar, desbloquear procesos si estaban bloqueados
    if (proc != NULL) {
        int nivel_int = fijar_nivel_int(NIVEL_3);
        proc->estado = LISTO;
        eliminar_elem(&lista_bloqueados_caracter, proc);
        insertar_ultimo(&lista_listos, proc);
        fijar_nivel_int(nivel_int);
        if (DEBUG == 1)
            printk("(INT_TERMINAL) Info: Se ha desbloqueado el proceso %d\n", proc->id);
    } else {
        if (DEBUG == 1)
            printk("(INT_TERMINAL) Error: No hay procesos esperando datos\n");
    }

        return;
}

/*
 * Tratamiento de interrupciones de reloj
 */
static void int_reloj() {

    reloj++;
    if (DEBUG == 1)
        printk("reloj = %d", reloj);
    //printk("-> TRATANDO INT. DE RELOJ\n");

    BCP *pActual = lista_bloqueados.primero;
    BCP *pAnterior = NULL;
    while (pActual != NULL) {
        if (pActual->t_dormir == reloj) {
            pAnterior = pActual;
            pActual = pActual->siguiente;
            eliminar_elem(&lista_bloqueados, pAnterior);
            insertar_ultimo(&lista_listos, pAnterior);
            pAnterior->estado = LISTO;
        } else {
            pActual = pActual->siguiente;
        }
    }

        return;
}

/*
 * Tratamiento de llamadas al sistema
 */
static void tratar_llamsis() {
    int nserv, res;

    nserv = leer_registro(0);
    if (nserv < NSERVICIOS)
        res = (tabla_servicios[nserv].fservicio)();
    else {
        if (DEBUG == 1)
            fprintf(stderr, "kernel.c -- tratar_llamsis -- Servicio no existente");
        res = -1;		/* servicio no existente */
    }
    escribir_registro(0, res);
    return;
}

/*
 * Tratamiento de interrupciuones software
 */
static void int_sw() {

    printk("-> TRATANDO INT. SW\n");

    return;
}

/*
 *
 * Funcion auxiliar que crea un proceso reservando sus recursos.
 * Usada por llamada crear_proceso.
 *
 */
static int crear_tarea(char *prog) {
    void * imagen, *pc_inicial;
    int error = 0;
    int proc;
    BCP *p_proc;

    proc = buscar_BCP_libre();
    if (proc == -1) {
        if (DEBUG == 1)
            fprintf(stderr, "kernel.c -- crear_tarea -- No hay entrada libre\n");
        return -1;	/* no hay entrada libre */
    }

    /* A rellenar el BCP ... */
    p_proc = &(tabla_procs[proc]);

    /* crea la imagen de memoria leyendo ejecutable */
    imagen = crear_imagen(prog, &pc_inicial);
    if (imagen) {
        p_proc->info_mem = imagen;
        p_proc->pila = crear_pila(TAM_PILA);
        fijar_contexto_ini(p_proc->info_mem, p_proc->pila, TAM_PILA,
            pc_inicial, &(p_proc->contexto_regs));
        p_proc->id = proc;
        p_proc->estado = LISTO;

        /* lo inserta al final de cola de listos */
        insertar_ultimo(&lista_listos, p_proc);
        error = 0;

        if (DEBUG == 1) {
            fprintf(stderr, "kernel.c -- crear_tarea -- LISTO -- PID = %d\n", p_proc->id);
        }
    }
    else {
        if (DEBUG == 1)
            fprintf(stderr, "kernel.c -- crear_tarea -- Fallo al crear imagen\n");
        error= -1; /* fallo al crear imagen */
    }

    return error;
}

/*
 *
 * Rutinas que llevan a cabo las llamadas al sistema
 *	sis_crear_proceso sis_escribir
 *
 */

/*
 * Tratamiento de llamada al sistema crear_proceso. Llama a la
 * funcion auxiliar crear_tarea sis_terminar_proceso
 */
int sis_crear_proceso() {
    char *prog;
    int res;

    printk("-> PROC %d: CREAR PROCESO\n", p_proc_actual->id);
    prog = (char *) leer_registro(1);
    if (DEBUG == 1)
        fprintf(stderr, "kernel.c -- sis_crear_proceso -- leer_registro(1)\n");
    res = crear_tarea(prog);
    if (DEBUG == 1)
        fprintf(stderr, "kernel.c -- sis_crear_proceso -- crear_tarea\n");
    return res;
}

/*
 * Tratamiento de llamada al sistema escribir. Llama simplemente a la
 * funcion de apoyo escribir_ker
 */
int sis_escribir() {
    char *texto;
    unsigned int longi;

    texto = (char *) leer_registro(1);
    longi = (unsigned int) leer_registro(2);

    escribir_ker(texto, longi);
    return 0;
}

/*
 * Tratamiento de llamada al sistema terminar_proceso. Llama a la
 * funcion auxiliar liberar_proceso
 */
int sis_terminar_proceso() {

    printk("-> FIN PROCESO %d\n", p_proc_actual->id);

    liberar_proceso();

        return 0; /* no deber�a llegar aqui */
}

/*
 * Devuelve el identificador del proceso que la invoca
 */
int obtener_id_pr() {
    return p_proc_actual->id;
}

int dormir(unsigned int segundos) {
    if (DEBUG == 1)
        fprintf(stderr, "kernel.c -- dormir -- PID = %d\n", p_proc_actual->id);
    segundos = (unsigned int) leer_registro(1);
    printk("tiempo a dormir = %d -- PID = %d\n", segundos * TICK, p_proc_actual->id);
    p_proc_actual->t_dormir = segundos * TICK + reloj;
    p_proc_actual->estado = BLOQUEADO;
    BCP *p_proc_anterior = p_proc_actual;
    //int nivel_int = fijar_nivel_int(3);
    eliminar_primero(&lista_listos);
    insertar_ultimo(&lista_bloqueados, p_proc_actual);
    p_proc_actual = planificador();
    //fijar_nivel_int(nivel_int);
    cambio_contexto(&(p_proc_anterior->contexto_regs), &(p_proc_actual->contexto_regs));
    return 0;
}

/* Funciones auxiliares para mutex */

static void iniciar_tabla_mutex() {
    int i;
    for (i = 0; i < NUM_MUT; i++) {
        tabla_mutex[i].estado = LIBRE; /* indica que el mutex esta libre */
    }
}

static int buscar_descriptor_libre() {
    int i;
    for (i = 0; i < NUM_MUT_PROC; i++) {
        if (p_proc_actual->descriptores_mutex[i] == NULL)
            return i; /* devuelve el n?mero del descriptor */
    }
    return -1; /* no hay descriptor libre */
}

static int buscar_nombre_mutex(char *nombre_mutex) {
    int i;
    if (DEBUGCREARMUTEX == 1)
        printk("(BUSCAR_NOMBRE_MUTEX...)\n");
    for (i = 0; i < NUM_MUT; i++) {
        //if (DEBUGCREARMUTEX == 1)
        //    printk("(BUSCAR_NOMBRE_MUTEX) Info: %s\n", tabla_mutex[i].nombre);
        if (tabla_mutex[i].nombre == NULL) {
            // Nothing to do, culpa de *nombre, fix con nombre[MAX_NOM_MUT)
        }
        else if (strcmp(tabla_mutex[i].nombre, nombre_mutex) == 0) {
            return i;
        }/* el nombre existe y devuelve su posicion en la tabla de mutex */
    }
    if (DEBUGCREARMUTEX == 1) {
        printk("(BUSCAR_NOMBRE_MUTEX) return -1...\n");
    }
    return -1; /* el nombre no existe */
}

static int buscar_mutex_libre() {
    int i;
    for (i = 0; i < NUM_MUT; i++) {
        if (tabla_mutex[i].estado == LIBRE)
            return i;	/* el mutex esta libre y devuelve su posicion en la tabla de mutex */
    }
    return -1; /* no hay mutex libre */
}

/*
 * Llamada al sistema crear_mutex
 * par�metro nombre en registro 1
 * par�metro tipo en registro 2
 */
int crear_mutex(char *nombre, int tipo) {
    if (DEBUGCREARMUTEX == 1)
        printk("(CREAR_MUTEX) Info: Creando mutex...\n");
    nombre = (char *) leer_registro(1);
    tipo = (int) leer_registro(2);
    if (DEBUGCREARMUTEX == 1) {
        printk("(CREAR_MUTEX) Info: Leidos los registros...\n");
        printk("%s\n", nombre);
    }

    if (strlen(nombre) > MAX_NOM_MUT) {
        printk("(SIS_CREAR_MUTEX) Error: Nombre de mutex demasiado largo\n");
        return -1;
    }
    if (DEBUGCREARMUTEX == 1)
        printk("(CREAR_MUTEX) Info: Nombre de mutex correcto...\n");

    if (buscar_nombre_mutex(nombre) >= 0) {
        printk("(SIS_CREAR_MUTEX) Error: Nombre de mutex ya existente\n");
        return -2;
    }
    if (DEBUGCREARMUTEX == 1)
        printk("(CREAR_MUTEX) Info: buscar_nombre_mutex(nombre) ok\n");

    int descr_mutex_libre = buscar_descriptor_libre();
    if (descr_mutex_libre < 0) {
        printk("(SIS_CREAR_MUTEX) Error: No hay descriptores de mutex libres\n");
        return -3;
    } else {
        printk("(SIS_CREAR_MUTEX) Devolviendo descriptor numero %d\n", descr_mutex_libre);
    }
    int pos_mutex_libre = buscar_mutex_libre();
    if (pos_mutex_libre < 0) {
        printk("(SIS_CREAR_MUTEX) No hay mutex libre, bloqueando proceso\n");
        // bloquear a la espera de que quede alguno libre
        BCP * proc_a_bloquear = p_proc_actual;
        proc_a_bloquear->estado = BLOQUEADO;
        int nivel_int = fijar_nivel_int(3);
        if (DEBUG == 1)
            printk("(SIS_CREAR_MUTEX) Eliminando proceso con PID %d de la cola de listos\n", proc_a_bloquear->id);
        eliminar_primero(&lista_listos);
        if (DEBUG == 1)
            printk("(SIS_CREAR_MUTEX) Insertando proceso con PID %d en la lista de bloqueados mutex libre\n", proc_a_bloquear-> id);
        insertar_ultimo(&lista_bloqueados_mutex_libre, proc_a_bloquear);
        p_proc_actual = planificador();
        fijar_nivel_int(nivel_int);
        cambio_contexto(&(proc_a_bloquear->contexto_regs),
                &(p_proc_actual->contexto_regs));
    } else {
        if (DEBUG == 1)
            printk("(CREAR_MUTEX) Info: Asignando mutex...\n");
        strcpy(tabla_mutex[pos_mutex_libre].nombre, nombre); // se copia el nombre al mutex
        if (DEBUG == 1)
            printk("(CREAR_MUTEX) Info: Copiado nombre mutex\n");
        tabla_mutex[pos_mutex_libre].tipo = tipo; // se asigna el tipo
        if (DEBUG == 1)
            printk("(CREAR_MUTEX) Info: Asignado tipo mutex\n");
        tabla_mutex[pos_mutex_libre].estado = OCUPADO; // se cambia el estado a OCUPADO
        if (DEBUG == 1)
            printk("(CREAR_MUTEX) Info: Cambiado estado mutex\n");
        tabla_mutex[pos_mutex_libre].num_bloqueos = 0;
        if (DEBUG == 1)
            printk("(CREAR_MUTEX) Info: Cambiado num_bloqueos\n");
        tabla_mutex[pos_mutex_libre].num_procesos_bloqueados = 0;
        if (DEBUG == 1)
            printk("(CREAR_MUTEX) Info: Cambiado num_procesos_bloqueados\n");
        p_proc_actual->descriptores_mutex[descr_mutex_libre] = &tabla_mutex[pos_mutex_libre]; // el descr apunta al mutex libre en la tabla
        if (DEBUG == 1)
            printk("(CREAR_MUTEX) Info: descriptores_mutex...\n");
    }
    return descr_mutex_libre;
}

int abrir_mutex(char* nombre) {
    nombre = (char*) leer_registro(1);
        int desc_mutex_libre = buscar_descriptor_libre();
        if (desc_mutex_libre < 0) {
            printk("(ABRIR_MUTEX) Error: NO hay descriptor de mutex libre\n");
            return -3;
        } else {
            printk("(ABRIR_MUTEX) Devolviendo descriptor numero %d\n", desc_mutex_libre);
        }
        int pos_nombre_mutex = buscar_nombre_mutex(nombre);
        if (pos_nombre_mutex < 0) {
            printk("(ABRIR_MUTEX) Error: No existe ningun mutex con nombre especificado \n"); // Error en que no existe el nombre especificada
            return -3;
        } else {
            p_proc_actual->descriptores_mutex[desc_mutex_libre] = pos_nombre_mutex; // el descr apunta al mutex con nombre especificada
        }
        return desc_mutex_libre;
}

int lock(unsigned int mutexid) {
    unsigned int descr_mutex = (unsigned int) leer_registro(1); // valor del descriptor de mutex al sistema lock se encuentra en el registro 1
    mutexid = buscar_mutex_valido(descr_mutex); // comprobar si es un valor v�lido
    if (mutexid == -1) {
        printk("(LOCK) Error:No es un descriptor v�lido\n");
        return -3;
    } else {
        int aux = &(p_proc_actual->descriptores_mutex[descr_mutex]);
        // si ningun proceso ha bloqueado con mutex
        if (tabla_mutex[aux].num_bloqueos == 0) {
            tabla_mutex[aux].num_bloqueos++; // proceso bloquea al recurso
            tabla_mutex[aux].pid_mutex = p_proc_actual->id;
        } else {
            // si ya ha sido bloqueado por proceso anteriormente
            if (tabla_mutex[aux].pid_mutex == p_proc_actual->id) {
                // el proceso que bloquea es el proceso actual
                if ((tabla_mutex[aux].tipo) == RECURSIVO) {
                    // si es recursivo bloquea de nuevo
                    tabla_mutex[aux].num_bloqueos++;
                } else {
                    // no es recursivo tenemos que avisar el error
                    printk("(LOCK) Error: no es un mutex recursivo\n");
                    return -3;
                }
            } else {
                // NO es el proceso actual el que bloquea
                BCP *proc_a_bloquear = p_proc_actual;
                // asignar proceso actual a proceso bloqueado
                proc_a_bloquear->estado = BLOQUEADO;
                tabla_mutex[aux].num_procesos_bloqueados++; // incrementamos el proceso en la espera del mutex en registro
                // cambiar el estado de proceso actual a bloqueado
                int nivel_int = fijar_nivel_int(3);
                eliminar_primero(&lista_listos); // eliminar el proceso actual de la lista de listo
                insertar_ultimo(&lista_bloqueados_mutex_lock, proc_a_bloquear); // a?adir el proceso a la lista de bloqueddo
                p_proc_actual = planificador();
                cambio_contexto(&(proc_a_bloquear->contexto_regs), &(p_proc_actual->contexto_regs));
                fijar_nivel_int(nivel_int);
            }
        }
    }
    return 0;
}

int unlock(unsigned int mutexid) {
    return 0;
}

int cerrar_mutex(unsigned int mutexid) {
    return 0;
}

int leer_caracter() {
    // Inhibir int_terminal
    int nivel_int2 = fijar_nivel_int(NIVEL_2);
    int caracter;

    if (num_caract_leidos == 0) {
        printk("(LEER_CARACTER) Info: No se ha introducido caracteres, bloqueando proceso...\n");
        int nivel_int3 = fijar_nivel_int(NIVEL_3);
        p_proc_actual->estado = BLOQUEADO;
        BCP *p_proc_anterior = p_proc_actual;
        eliminar_primero(&lista_listos);
        insertar_ultimo(&lista_bloqueados_caracter, p_proc_actual);
        p_proc_actual = planificador();
        cambio_contexto(&(p_proc_anterior->contexto_regs), &(p_proc_actual->contexto_regs));
        fijar_nivel_int(nivel_int3);
    }

    caracter = buffer_terminal[primero];
    primero++;
    num_caract_leidos--;
    if (primero == TAM_BUF_TERM) {
        primero = 0;
    }
    fijar_nivel_int(nivel_int2);

    return caracter;
}


/*
 *
 * Rutina de inicializaci�n invocada en arranque
 *
 */
int main() {
    /* se llega con las interrupciones prohibidas */

    instal_man_int(EXC_ARITM, exc_arit);
    instal_man_int(EXC_MEM, exc_mem);
    instal_man_int(INT_RELOJ, int_reloj);
    instal_man_int(INT_TERMINAL, int_terminal);
    instal_man_int(LLAM_SIS, tratar_llamsis);
    instal_man_int(INT_SW, int_sw);

    iniciar_cont_int();		/* inicia cont. interr. */
    iniciar_cont_reloj(TICK);	/* fija frecuencia del reloj */
    iniciar_cont_teclado();		/* inici cont. teclado */

    iniciar_tabla_proc();		/* inicia BCPs de tabla de procesos */
    iniciar_tabla_mutex();      /* inicia tabla mutex */

    /* crea proceso inicial */
    if (crear_tarea((void *) "init") < 0)
        panico("no encontrado el proceso inicial");

    /* activa proceso inicial */
    p_proc_actual = planificador();
    cambio_contexto(NULL, &(p_proc_actual->contexto_regs));
    panico("S.O. reactivado inesperadamente");
    return 0;
}
